﻿#define _CRT_SECURE_NO_WARNINGS
#include <conio.h>
#include <windows.h>
#include <tchar.h>
#include "fileOwner.h"

int main(int argc, char* argv[])
{
	if (argc < 2) {
		_tprintf(TEXT("Programm has file path param. Please try again and enter it\n"));
		return -1;
	}

	FileOwner* fileOwner = new FileOwner(argv[1]);
	if (fileOwner->find() == NULL) {
		_tprintf(TEXT("INTERNAL_SERVER_ERROR\n"));
		delete fileOwner;
		return -1;
	}
	delete fileOwner;
	return 0;
}
