#include "fileOwner.h"

FileOwner::FileOwner(char* _sFile)
{
	strcpy(this->sFile, _sFile);
}

FileOwner::~FileOwner()
{
	delete[] sFile;
}

LPTSTR FileOwner::find() 
{
	return checkAnGetOwner();
}

LPTSTR FileOwner::checkAnGetOwner()
{
	if (getFileHandle()
		&& getOwnerSID()
		&& !getAccountBySID()
		&& allocateBytes()
		&& getAccountBySID()
		&& checkAndPrintAccount()) {
		return AcctName;
	}
	return NULL;
}

bool FileOwner::getFileHandle()
{
	// ��������� ����������� �����
	hFile = CreateFileA(
		sFile,					// ���� ��� ��� �����
		GENERIC_READ,			// ������ ������� � ����������� �����
		FILE_SHARE_READ,		// ��� ����������� �������
		NULL,					// �������� ������������
		OPEN_EXISTING,			// ��������, ����������� ��� ������
		FILE_ATTRIBUTE_NORMAL,	// �������� � �����
		NULL					// ���������� ����� �������
	);

	// ��������� ������ ��� ���������� ������� CreateFileA
	if (hFile == INVALID_HANDLE_VALUE) {
		_tprintf(TEXT("CreateFile error = %d\n"), GetLastError());
		return FALSE;
	}

	return TRUE;
}

bool FileOwner::getOwnerSID()
{
	// ��������� SID ������������ �����
	DWORD dwRtnCode = GetSecurityInfo(
		hFile,						// ���������� �����
		SE_FILE_OBJECT,				// ��� ������� (��������� ���� ��� �������)
		OWNER_SECURITY_INFORMATION,	// ���������� ��� ���������� ������������ (������ �� ������������� ��������� �������)
		&pSidOwner,					// ��������� �� ����������, ������� �������� ��������� �� SID  (Security Identifier) ��������� � ����������� ������������, ������������ � ppSecurityDescriptor
		NULL,						// ��������� �� ����������, ������� �������� ��������� �� SID  (Security Identifier) ��������� ������ � ������������ ����������� ������������
		NULL,						// ��������� �� ����������, ������� �������� ��������� �� DACL (Discretionary Access Control List) � ������������ ����������� ������������
		NULL,						// ��������� �� ����������, ������� �������� ��������� �� SACL (System Access Control List) � ������������ ����������� ������������
		&pSD						// ��������� �� ����������, ������� �������� ��������� �� ���������� ������������ �������
	);

	// ��������� ������ ��� ���������� ������� GetSecurityInfo
	if (dwRtnCode != ERROR_SUCCESS) {
		_tprintf(TEXT("GetSecurityInfo error = %d\n"), GetLastError());
		return FALSE;
	}

	return TRUE;
}

bool FileOwner::getAccountBySID()
{
	// ���������� ����� ������� ������ ��� ���������� SID � ��� ������� ������, � ������� ���� SID ������
	bRtnBool = LookupAccountSid(
		NULL,					// ��������� �������
		pSidOwner,				// ��������� �� ����������, ���������� SID
		AcctName,				// ��������� �� �����, ������� �������� ������ � ����������� �����, ���������� ��� ������� ������, ��������������� ��������� lpSid
		(LPDWORD)&dwAcctName,	// ������ ������ ����� ������� ������
		DomainName,				// ��������� �� �����, ������� �������� ������ � ����������� �����, ���������� ��� ������, � ������� ���� ������� ��� ������� ������
		(LPDWORD)&dwDomainName,	// ������ ������ ����� ������
		&eUse					// ��� ������� ������
	);

	return bRtnBool;
}

bool FileOwner::allocateBytes()
{
	// ��������� ���������� ���������� ���� �� ����
	AcctName = (LPTSTR)GlobalAlloc(
		GMEM_FIXED,				// ������� ��������� ������ (������������� ���������)
		dwAcctName				// ���������� ���������� ����
	);

	// ��������� ������ ��� ���������� ������� GlobalAlloc
	if (AcctName == NULL) {
		_tprintf(TEXT("GlobalAlloc error = %d\n"), GetLastError());
		return FALSE;
	}

	// ��������� ���������� ���������� ���� �� ����
	DomainName = (LPTSTR)GlobalAlloc(
		GMEM_FIXED,				// ������� ��������� ������ (������������� ���������)
		dwDomainName			// ���������� ���������� ����
	);

	// ��������� ������ ��� ���������� ������� GlobalAlloc
	if (DomainName == NULL) {
		_tprintf(TEXT("GlobalAlloc error = %d\n"), GetLastError());
		return FALSE;

	}

	return TRUE;
}

bool FileOwner::checkAndPrintAccount()
{
	if (bRtnBool == FALSE) {

		if (GetLastError() == ERROR_NONE_MAPPED)
		{
			_tprintf(TEXT
			("Account owner not found for specified SID.\n"));
		}
		else
		{
			_tprintf(TEXT("Error in LookupAccountSid = %d\n"), GetLastError());
		}
		return FALSE;

	}
	else if (bRtnBool == TRUE)
	{
		_tprintf(TEXT("Account owner = %s\nDomain owner  = %s\n"), AcctName, DomainName);
		return TRUE;
	}
}