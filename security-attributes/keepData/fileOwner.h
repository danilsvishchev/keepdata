#pragma once
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <conio.h>
#include <windows.h>
#include <tchar.h>
#include "accctrl.h"
#include "aclapi.h"
#pragma comment(lib, "advapi32.lib")

class FileOwner
{
public:
	FileOwner(char* _sFile);

	~FileOwner();

	LPTSTR find();

private:
	char* sFile = new char[255];
	HANDLE hFile;
	PSID pSidOwner = NULL;
	BOOL bRtnBool = TRUE;
	LPTSTR AcctName = NULL;
	LPTSTR DomainName = NULL;
	DWORD dwAcctName = 1, dwDomainName = 1;
	SID_NAME_USE eUse = SidTypeUnknown;
	PSECURITY_DESCRIPTOR pSD = NULL;

	LPTSTR checkAnGetOwner();

	bool getFileHandle();

	bool getOwnerSID();

	bool getAccountBySID();

	bool allocateBytes();

	bool checkAndPrintAccount();
};


